const express = require("express");
const router = express.Router();

const taskController = require("../controllers/tasks");

router.get("/contacts", taskController.contactsData);

router.get("/update", taskController.getContactByEmail);

router.post("/update", taskController.updateBook);

module.exports = router;
