const axios = require("axios"); //require axios

const baseUrl = "https://api.hubapi.com"; //base url for HubSpot API
const PRIVATE_APP_ACCESS = process.env.PRIVATE_APP_ACCESS; // ACCESS KEY for Hubspot App

//Get all Contacts API
const contactsData = async (req, res) => {
  const contacts = `${baseUrl}/crm/v3/objects/contacts`;
  const headers = {
    Authorization: `Bearer ${PRIVATE_APP_ACCESS}`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(contacts, { headers });
    const data = resp.data.results;
    res.render("contacts", { title: "Contacts | Hubspot APIs", data });
  } catch (error) {
    console.log(error);
  }
};

// Get API to get Properties of contact from Email as query parameter
const getContactByEmail = async (req, res) => {
  const email = req.query.email;
  const getContact = `${baseUrl}/crm/v3/objects/contacts/${email}?idProperty=email&properties=email,favorite_book`;
  const headers = {
    Authorization: `Bearer ${PRIVATE_APP_ACCESS}`,
    "Content-Type": "application/json",
  };
  try {
    const response = await axios.get(getContact, { headers });
    const data = response.data;
    // res.json(data);
    res.render("update", {
      userEmail: data.properties.email,
      favoriteBook: data.properties.favorite_book,
    });
  } catch (error) {
    console.log(error);
  }
};

//Post API to update favorite book
// const updateBook = async (req, res) => {
//   const update = {
//     properties: {
//       favorite_book: req.body.newVal,
//     },
//   };
//   const email = req.query.email;
//   const updateContact = `${baseUrl}/crm/v3/objects/contacts/${email}?idProperty=email`;
//   const headers = {
//     Authorization: `Bearer ${PRIVATE_APP_ACCESS}`,
//     "Content-Type": "application/json",
//   };
//   try {
//     await axios.patch(updateContact, update, { headers });
//     res.redirect("back");
//   } catch (error) {
//     console.log(error);
//   }
// };

// export controller functions
module.exports = { contactsData, getContactByEmail, updateBook };
