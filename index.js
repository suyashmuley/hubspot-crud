require("dotenv").config(); // include dotenv
const express = require("express"); // include express
const PORT = 5000; // Port 5000 for the server
const routes = require("./routes/tasks"); // import routes

const app = express();

//set engine as Pug templating language
app.set("view engine", "pug");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/", routes); // use routes

//Listening on port 5000
app.listen(PORT, () => console.log(`Listening on http://localhost:${PORT}`));
